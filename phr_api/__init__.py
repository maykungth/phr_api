__author__ = 'Maykungth'
from flask import Flask
from flask.ext.cors import CORS
from flask_restful import Resource,Api

app = Flask(__name__)
CORS(app)
apirest = Api(app)

app.config['UPLOAD_FOLDER'] = '/home/hduser/uploads'    #
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/hduser/authen/authen.db'

app.config['SECURITY_PASSWORD_HASH'] = 'pbkdf2_sha512'
app.config['SECURITY_TRACKABLE'] = True
app.config['SECURITY_PASSWORD_SALT'] = 'something_super_secret_change_in_production'
app.config['WTF_CSRF_ENABLED'] = False

MasterHbase = '172.30.224.142'  #HBase Master
Master = '172.30.224.137'       #HDFS Master
largeSize = 10000000            #10MB
HDFSMainPath ='/DSePHR/'